package com.example.sportloka.models;

public class Area {

    private String idArea;
    private String nameArea;
    private String location;
    private String latitude;
    private String longitude;
    private String reviewTotal;
    private String rating;
    private String price;
    private String days;
    private String image;
    private String rekening;

    public Area(String idArea, String nameArea, String location, String latitude, String longitude, String reviewTotal, String rating, String price, String days, String image, String rekening) {
        this.idArea = idArea;
        this.nameArea = nameArea;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.reviewTotal = reviewTotal;
        this.rating = rating;
        this.price = price;
        this.days = days;
        this.image = image;
        this.rekening = rekening;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getNameArea() {
        return nameArea;
    }

    public void setNameArea(String nameArea) {
        this.nameArea = nameArea;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getReviewTotal() {
        return reviewTotal;
    }

    public void setReviewTotal(String reviewTotal) {
        this.reviewTotal = reviewTotal;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRekening() {
        return rekening;
    }

    public void setRekening(String rekening) {
        this.rekening = rekening;
    }
}
