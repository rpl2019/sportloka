package com.example.sportloka.models;

public class Booking {
    private String date;
    private String email;
    private String idArea;
    private String idBooking;
    private String latitude;
    private String longitude;
    private String loc;
    private String nameArea;
    private String schedule;
    private String photo;
    private String dateBooking;
    private String status;
    private String note;
    private String price;
    private String account;

    public Booking(String date, String email, String idArea, String idBooking, String latitude, String longitude, String loc, String nameArea, String schedule, String photo, String dateBooking, String status, String note, String price, String account) {
        this.date = date;
        this.email = email;
        this.idArea = idArea;
        this.idBooking = idBooking;
        this.latitude = latitude;
        this.longitude = longitude;
        this.loc = loc;
        this.nameArea = nameArea;
        this.schedule = schedule;
        this.photo = photo;
        this.dateBooking = dateBooking;
        this.status = status;
        this.note = note;
        this.price = price;
        this.account = account;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(String idBooking) {
        this.idBooking = idBooking;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getNameArea() {
        return nameArea;
    }

    public void setNameArea(String nameArea) {
        this.nameArea = nameArea;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDateBooking() {
        return dateBooking;
    }

    public void setDateBooking(String dateBooking) {
        this.dateBooking = dateBooking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
