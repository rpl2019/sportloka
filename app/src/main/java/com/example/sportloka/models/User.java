package com.example.sportloka.models;

public class User {
    private String name;
    private String email;
    private String phone;
    private String typeLogin;
    private String photo;

    public User(String name, String email, String phone, String typeLogin, String photo) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.typeLogin = typeLogin;
        this.photo = photo;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTypeLogin() {
        return typeLogin;
    }

    public void setTypeLogin(String typeLogin) {
        this.typeLogin = typeLogin;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
