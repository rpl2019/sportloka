package com.example.sportloka.home.detailArea;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sportloka.MapsActivity;
import com.example.sportloka.R;
import com.example.sportloka.models.Area;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class DetailAreaActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnBack, btnBooking;
    private TextView tvNameArea, tvLocation, tvReview, tvPrice, tvSchedule;
    private RatingBar rating;
    private Area area;
    private SharedPreferences sharedPreferences;
    private User user;
    private FirebaseFirestore mDatabase;
    private FirebaseAuth mAuth;
    private String[] schedule = new String[]{"08.00-09.00","09.00-10.00","10.00-11.00","11.00-12.00","12.00-13.00","13.00-14.00","14.00-15.00","15.00-16.00","16.00-17.00","17.00-18.00","18.00-19.00","19.00-20.00","20.00-21.00","21.00-22.00"};
    private boolean[] checked = new boolean[schedule.length];
    private Gson gson = new Gson();
    private ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_area);

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnBooking = findViewById(R.id.btn_booking);
        btnBooking.setOnClickListener(this);
        tvNameArea = findViewById(R.id.tv_area_name);
        tvLocation = findViewById(R.id.tv_location);
        rating = findViewById(R.id.rtb);
        tvReview = findViewById(R.id.tv_review);
        tvPrice = findViewById(R.id.tv_price_hour);
        tvSchedule = findViewById(R.id.tv_schedule);
        imageView = findViewById(R.id.iv_maps);
        imageView.setOnClickListener(this);

        mDatabase = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        sharedPreferences = getSharedPreferences("userProfile", Context.MODE_PRIVATE);

        Bundle dataParse = getIntent().getExtras();
        String json = dataParse.getString("area");
        area = gson.fromJson(json, Area.class);

        setupView();
    }

    private void setupView() {
        tvNameArea.setText(area.getNameArea());
        tvLocation.setText(area.getLocation());
        rating.setRating(Float.valueOf(area.getRating()));
        tvReview.setText(area.getReviewTotal());
        tvPrice.setText(area.getPrice() + "/h");

        String day = area.getDays().replace("\"", "");
        String day1 = day.replace("[", "");
        String day2 = day1.replace("]", "");
        String mDay = day2.replace(",", ", ");
        tvSchedule.setText(mDay);
    }

    private void saveBooking(String values, String date, String price) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        user = gson.fromJson(json, User.class);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd kk:mm", Locale.getDefault());
        String strDate = mdformat.format(calendar.getTime());

        final Map<String, Object> booking = new HashMap<>();
        booking.put("date", strDate);
        booking.put("email", user.getEmail());
        booking.put("idArea", area.getIdArea());
        booking.put("idBooking", user.getEmail() + " " + strDate);
        booking.put("latitude", area.getLatitude());
        booking.put("longitude", area.getLongitude());
        booking.put("location", area.getLocation());
        booking.put("nameArea", area.getNameArea());
        booking.put("schedule", values);
        booking.put("photo", area.getImage());
        booking.put("dateBooking", date);
        booking.put("status", "waiting verification");
        booking.put("note", "-");
        booking.put("price", price);
        booking.put("rekening", area.getRekening());

        mDatabase.collection("Booking")
                .add(booking)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(), "Booking Success", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void selectSchedule(final String vDate) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailAreaActivity.this);
        builder.setTitle("Select Schedule");
        builder.setMultiChoiceItems(schedule, checked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                // Update the current focused item's checked status
                checked[which] = isChecked;
            }
        });
        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<String> values = new ArrayList<>();

                for (int i = 0; i < checked.length; i++ ) {
                    if (checked[i] == true) {
                        values.add(schedule[i]);
                    }
                }

                long price = values.size() * Integer.parseInt(area.getPrice());

                if (values.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Choose Schedule", Toast.LENGTH_LONG).show();
                } else {
                    Gson gson = new Gson();
                    String stringValue = gson.toJson(values);
                    saveBooking(stringValue, vDate, String.valueOf(price));
                }
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        // Set the neutral/cancel button click listener
        builder.setNeutralButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the neutral button
                for (int i = 0; i < checked.length; i++) {
                    checked[i] = false;
                }
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    private void selectDate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final DatePicker picker = new DatePicker(this);
        picker.setCalendarViewShown(false);

        builder.setTitle("Select Date");
        builder.setView(picker);
        // Set the positive/yes button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String day = String.valueOf(picker.getDayOfMonth());
                String month = String.valueOf(picker.getMonth());
                String year = String.valueOf(picker.getYear());
//                Toast.makeText(getApplicationContext(), day + "-" + month + "-" + year, Toast.LENGTH_LONG).show();
                selectSchedule(day + "-" + month + "-" + year);
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_booking:
                selectDate();
                break;
            case R.id.btn_back:
                super.onBackPressed();
                break;
            case R.id.iv_maps:
                String json = gson.toJson(area);
                startActivity(new Intent(getApplicationContext(), MapsActivity.class).putExtra("area", json));
                break;
        }
    }
}
