package com.example.sportloka.home.listArea;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sportloka.R;
import com.example.sportloka.home.RecommendListAdapter;
import com.example.sportloka.models.Area;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListAreaActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RecommendListAdapter adapter;
    private FirebaseFirestore mDatabase;
    private ArrayList<Area> areaList;
    private ProgressBar progressBar;
    private String idAreaParse;
    private TextView titleBar;
    private Button btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_area);

        mDatabase = FirebaseFirestore.getInstance();
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnBack.setVisibility(View.VISIBLE);

        Bundle dataParse = getIntent().getExtras();
        idAreaParse = dataParse.getString("idArea");

        getData();

        setupView();
    }

    private void setupView() {

        titleBar = findViewById(R.id.title_bar);

        if (idAreaParse.equals("abs")) {
            titleBar.setText("BASKET AREA");
        } else if (idAreaParse.equals("fts")) {
            titleBar.setText("FUTSAL AREA");
        } else {
            titleBar.setText("BADMINTON AREA");
        }

        areaList = new ArrayList<>();

        recyclerView = findViewById(R.id.rv_list_area);
        adapter = new RecommendListAdapter(areaList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }

    private void getData() {
        areaList = new ArrayList<>();

        mDatabase.collection("Area")
                .orderBy("rating", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String idArea = document.getData().get("idArea").toString();
                                String nameArea = document.getData().get("nameArea").toString();
                                String location = document.getData().get("location").toString();
                                String latitude = document.getData().get("latitude").toString();
                                String longitude = document.getData().get("longitude").toString();
                                String reviewTotal = document.getData().get("reviewTotal").toString();
                                String rating = document.getData().get("rating").toString();
                                String price = document.getData().get("price").toString();
                                String days = document.getData().get("days").toString();
                                String image = document.getData().get("image").toString();
                                String rekening = document.getData().get("rekening").toString();

                                if (idArea.contains(idAreaParse)) {
                                    Area b = new Area(idArea, nameArea, location, latitude, longitude, reviewTotal, rating, price, days, image, rekening);
                                    areaList.add(b);
                                }

                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                super.onBackPressed();
        }
    }
}

