package com.example.sportloka.home;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.sportloka.R;
import com.example.sportloka.home.detailArea.DetailAreaActivity;
import com.example.sportloka.models.Area;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecommendListAdapter extends RecyclerView.Adapter<RecommendListAdapter.RecommendListViewHolder> {

    private ArrayList<Area> dataList;

    public RecommendListAdapter(ArrayList<Area> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecommendListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_recommend, parent, false);
        return new RecommendListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecommendListViewHolder holder, final int position) {
        StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl("gs://sportloka-81187.appspot.com/").child(dataList.get(position).getIdArea() + ".jpg");
        holder.areaName.setText(dataList.get(position).getNameArea());
        holder.rating.setRating(Float.valueOf(dataList.get(position).getRating()));
        holder.review.setText(dataList.get(position).getReviewTotal() + " Reviews");

        storageReference.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Glide.with(holder.image.getContext()).load(task.getResult()).placeholder(R.drawable.placeholder_sport).into(holder.image);
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String json = gson.toJson(dataList.get(position));
                v.getContext().startActivity(new Intent(v.getContext(), DetailAreaActivity.class).putExtra("area", json));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class RecommendListViewHolder extends RecyclerView.ViewHolder{

        private ImageView image;
        private TextView areaName;
        private RatingBar rating;
        private TextView review;

        public RecommendListViewHolder(View itemView) {
            super(itemView);

            areaName = itemView.findViewById(R.id.tv_name);
            image = itemView.findViewById(R.id.iv_area);
            rating = itemView.findViewById(R.id.rtb);
            review = itemView.findViewById(R.id.tv_review);
        }
    }
}
