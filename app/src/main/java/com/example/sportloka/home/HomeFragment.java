package com.example.sportloka.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sportloka.MapsActivity;
import com.example.sportloka.R;
import com.example.sportloka.history.HistoryListAdapter;
import com.example.sportloka.home.listArea.ListAreaActivity;
import com.example.sportloka.models.Area;
import com.example.sportloka.models.Booking;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class HomeFragment extends Fragment implements View.OnClickListener{

    private RecyclerView listBasket, listFutsal, listBadminton;
    private RecommendListAdapter adapterBasket, adapterFutsal, adapterBadminton;
    private ArrayList<Area> basketList, badmintonList, futsalList;
    private FirebaseFirestore mDatabase;
    private FirebaseAuth mAuth;
    private ProgressBar progressBarBadminton, progressBarBasket, progressBarFutsal;
    private LinearLayout btnBasket, btnBadminton, btnFutsal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        progressBarBadminton = view.findViewById(R.id.progressBarBadminton);
        progressBarBadminton.setVisibility(View.GONE);
        progressBarBasket = view.findViewById(R.id.progressBarBasket);
        progressBarBasket.setVisibility(View.GONE);
        progressBarFutsal = view.findViewById(R.id.progressBarFutsal);
        progressBarFutsal.setVisibility(View.GONE);
        btnBadminton = view.findViewById(R.id.ly_menu_badminton);
        btnBadminton.setOnClickListener(this);
        btnBasket = view.findViewById(R.id.ly_menu_basket);
        btnBasket.setOnClickListener(this);
        btnFutsal = view.findViewById(R.id.ly_menu_futsal);
        btnFutsal.setOnClickListener(this);

//        addData();

        setupListRecommended(view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        getDataRecommendedBasket();
        getDataRecommendedBadminton();
        getDataRecommendedFutsal();
    }

    private void setupListRecommended(View view) {
        //SETUP LAYOUT MANAGER
        RecyclerView.LayoutManager layoutManagerBasket, layoutManagerFutsal, layoutManagerBadminton;

        layoutManagerBasket = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerFutsal = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerBadminton = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        //SETUP LIST BASKET
        listBasket = view.findViewById(R.id.rv_recom_basket);

        adapterBasket = new RecommendListAdapter(basketList);

        listBasket.setLayoutManager(layoutManagerBasket);

        listBasket.setAdapter(adapterBasket);

        //SETUP LIST BADMINTON
        listBadminton = view.findViewById(R.id.rv_recom_badminton);

        adapterBadminton = new RecommendListAdapter(badmintonList);

        listBadminton.setLayoutManager(layoutManagerBadminton);

        listBadminton.setAdapter(adapterBadminton);

        //SETUP LIST FUTSAL
        listFutsal = view.findViewById(R.id.rv_recom_futsal);

        adapterFutsal = new RecommendListAdapter(futsalList);

        listFutsal.setLayoutManager(layoutManagerFutsal);

        listFutsal.setAdapter(adapterFutsal);
    }

    private void getDataRecommendedBasket() {
        basketList = new ArrayList<>();

        mDatabase.collection("Area")
                .orderBy("rating", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String idArea = document.getData().get("idArea").toString();
                                String nameArea = document.getData().get("nameArea").toString();
                                String location = document.getData().get("location").toString();
                                String latitude = document.getData().get("latitude").toString();
                                String longitude = document.getData().get("longitude").toString();
                                String reviewTotal = document.getData().get("reviewTotal").toString();
                                String rating = document.getData().get("rating").toString();
                                String price = document.getData().get("price").toString();
                                String days = document.getData().get("days").toString();
                                String image = document.getData().get("image").toString();
                                String rekening = document.getData().get("rekening").toString();

                                if ((idArea.contains("abs")) && (basketList.size() < 3)) {
                                    Area b = new Area(idArea, nameArea, location, latitude, longitude, reviewTotal, rating, price, days, image, rekening);
//                                Toast.makeText(getContext(), b.getNameArea(), Toast.LENGTH_LONG).show();
                                    basketList.add(b);
                                }

                                adapterBasket.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBarBasket.setVisibility(View.GONE);
                    }
                });
    }

    private void getDataRecommendedBadminton() {
        badmintonList = new ArrayList<>();

        mDatabase.collection("Area")
                .orderBy("rating", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String idArea = document.getData().get("idArea").toString();
                                String nameArea = document.getData().get("nameArea").toString();
                                String location = document.getData().get("location").toString();
                                String latitude = document.getData().get("latitude").toString();
                                String longitude = document.getData().get("longitude").toString();
                                String reviewTotal = document.getData().get("reviewTotal").toString();
                                String rating = document.getData().get("rating").toString();
                                String price = document.getData().get("price").toString();
                                String days = document.getData().get("days").toString();
                                String image = document.getData().get("image").toString();
                                String rekening = document.getData().get("rekening").toString();

                                if ((idArea.contains("bdm")) && (badmintonList.size() < 3)) {
                                    Area b = new Area(idArea, nameArea, location, latitude, longitude, reviewTotal, rating, price, days, image, rekening);
//                                Toast.makeText(getContext(), b.getNameArea(), Toast.LENGTH_LONG).show();
                                    badmintonList.add(b);
                                }

                                adapterBadminton.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBarBadminton.setVisibility(View.GONE);
                    }
                });
    }

    private void getDataRecommendedFutsal() {
        futsalList = new ArrayList<>();

        mDatabase.collection("Area")
                .orderBy("rating", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String idArea = document.getData().get("idArea").toString();
                                String nameArea = document.getData().get("nameArea").toString();
                                String location = document.getData().get("location").toString();
                                String latitude = document.getData().get("latitude").toString();
                                String longitude = document.getData().get("longitude").toString();
                                String reviewTotal = document.getData().get("reviewTotal").toString();
                                String rating = document.getData().get("rating").toString();
                                String price = document.getData().get("price").toString();
                                String days = document.getData().get("days").toString();
                                String image = document.getData().get("image").toString();
                                String rekening = document.getData().get("rekening").toString();

                                if ((idArea.contains("fts")) && (futsalList.size() < 3)) {
                                    Area b = new Area(idArea, nameArea, location, latitude, longitude, reviewTotal, rating, price, days, image, rekening);
//                                Toast.makeText(getContext(), b.getNameArea(), Toast.LENGTH_LONG).show();
                                    futsalList.add(b);
                                }

                                adapterFutsal.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBarFutsal.setVisibility(View.GONE);
                    }
                });
    }


    void addData(){
//        FOR CREATE DATA DUMMY
        ArrayList<String> days = new ArrayList<>();
        days.add("Sunday");
//        days.add("Monday");
//        days.add("Tuesday");
        days.add("Wednesday");
//        days.add("Thursday");
        days.add("Friday");
//        days.add("All Days");

        Gson gson = new Gson();
        String stringDays = gson.toJson(days);

        final Map<String, Object> basket = new HashMap<>();
        basket.put("idArea", "abs_00005");
        basket.put("nameArea", "Basket Area 5");
        basket.put("location", "Jl. Sukabirus no. 134");
        basket.put("latitude", "-6.914744");
        basket.put("longitude", "107.609810");
        basket.put("reviewTotal", "506");
        basket.put("rating", 5);
        basket.put("price", "96000");
        basket.put("days", stringDays);
        basket.put("image", "gs://sportloka-81187.appspot.com/abs_00005.jpg");

        final Map<String, Object> badminton = new HashMap<>();
        badminton.put("idArea", "bdm_00005");
        badminton.put("nameArea", "Badminton Area 5");
        badminton.put("location", "Jl. Sukabirus no. 134");
        badminton.put("latitude", "-6.914744");
        badminton.put("longitude", "107.609810");
        badminton.put("reviewTotal", "175");
        badminton.put("rating", 4.5);
        badminton.put("price", "58000");
        badminton.put("days", stringDays);
        badminton.put("image", "gs://sportloka-81187.appspot.com/bdm_00005.jpg");

        final Map<String, Object> futsal = new HashMap<>();
        futsal.put("idArea", "fts_00005");
        futsal.put("nameArea", "Futsal Area 5");
        futsal.put("location", "Jl. Sukabirus no. 134");
        futsal.put("latitude", "-6.914744");
        futsal.put("longitude", "107.609810");
        futsal.put("reviewTotal", "607");
        futsal.put("rating", 4.5);
        futsal.put("price", "190000");
        futsal.put("days", stringDays);
        futsal.put("image", "gs://sportloka-81187.appspot.com/fts_00005.jpg");

        mDatabase.collection("Area")
                .add(badminton)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

        mDatabase.collection("Area")
                .add(basket)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

        mDatabase.collection("Area")
                .add(futsal)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_menu_badminton:
                startActivity(new Intent(getActivity(), ListAreaActivity.class).putExtra("idArea", "bdm"));
                break;
            case R.id.ly_menu_basket:
                startActivity(new Intent(getActivity(), ListAreaActivity.class).putExtra("idArea", "abs"));
                break;
            case R.id.ly_menu_futsal:
                startActivity(new Intent(getActivity(), ListAreaActivity.class).putExtra("idArea", "fts"));
                break;
        }
    }
}
