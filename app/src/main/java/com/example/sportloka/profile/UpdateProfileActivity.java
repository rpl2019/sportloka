package com.example.sportloka.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sportloka.R;
import com.example.sportloka.models.Area;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatEditText;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView titleBar;
    private Button backBtn, updateBtn;
    private AppCompatEditText etName, etPhone;
    private FirebaseFirestore mDatabase;
    private SharedPreferences sharedPreferences;
    private ProgressBar progressBar;
    private User user;
    private String idDoc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.activity_edit_profile);

        sharedPreferences = getSharedPreferences("userProfile", Context.MODE_PRIVATE);
        mDatabase = FirebaseFirestore.getInstance();

        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        user = gson.fromJson(json, User.class);

        titleBar = findViewById(R.id.title_bar);
        backBtn = findViewById(R.id.btn_back);
        updateBtn = findViewById(R.id.btn_update);
        etName = findViewById(R.id.et_name);
        etPhone = findViewById(R.id.et_phone);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        titleBar.setText("PROFILE");
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(this);
        updateBtn.setOnClickListener(this);

        getProfile();
    }

    boolean validate_field(String name, String phone){
        //CHECK FIELD SERVICE
        if(name.isEmpty()){
            etName.setError("Please enter your service");
            etName.requestFocus();
            return false;
        }

        //CHECK FIELD COMPLAIN
        if(phone.isEmpty()){
            etPhone.setError("Please enter your complain");
            etPhone.requestFocus();
            return false;
        }

        return  true;
    }

    private void getProfile() {
        mDatabase.collection("User")
                .whereEqualTo("email", user.getEmail())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                idDoc = document.getId();

                                String name = document.getData().get("name").toString();
                                String phone = document.getData().get("phone").toString();

                                etName.setText(name);
                                etPhone.setText(phone);

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void updateProfile(final String name, String phone) {
        progressBar.setVisibility(View.VISIBLE);

        final Map<String, Object> com = new HashMap<>();
        com.put("name", name);
        com.put("phone", phone);

        mDatabase.collection("User")
                .document(idDoc)
                .update(com)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Update Success", Toast.LENGTH_SHORT).show();

                            Gson gson = new Gson();
                            String classJson = sharedPreferences.getString("user", null);
                            user = gson.fromJson(classJson, User.class);

                            User userUpdate = new User();
                            userUpdate.setEmail(user.getEmail());
                            userUpdate.setName(name);
                            userUpdate.setPhone(user.getPhone());
                            userUpdate.setPhoto(user.getPhoto());
                            userUpdate.setTypeLogin(user.getTypeLogin());

                            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

                            String json = gson.toJson(userUpdate);
                            prefsEditor.putString("user", json);
                            prefsEditor.apply();

                            progressBar.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(getApplicationContext(), "Update failed", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                super.onBackPressed();
                break;
            case R.id.btn_update:
                String name = etName.getText().toString().trim();
                String phone = etPhone.getText().toString().trim();

                if (validate_field(name, phone)) {
                    etPhone.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    updateProfile(name, phone);
                }
                break;
        }
    }
}
