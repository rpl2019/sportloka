package com.example.sportloka.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sportloka.auth.LoginActivity;
import com.example.sportloka.R;
import com.example.sportloka.models.User;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private Button btnEditProfile, btnComplain, btnAboutUs, btnSignOut;
    private TextView tvName;
    private CircleImageView ivProfile;
    private FirebaseAuth auth;
    private FirebaseFirestore mDatabase;
    private GoogleSignInClient mGoogleSignInClient;
    private SharedPreferences sharedPreferences;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        btnEditProfile = view.findViewById(R.id.btn_edit_profile);
        btnEditProfile.setOnClickListener(this);
        btnComplain = view.findViewById(R.id.btn_complain);
        btnComplain.setOnClickListener(this);
        btnAboutUs = view.findViewById(R.id.btn_about_us);
        btnAboutUs.setOnClickListener(this);
        btnSignOut = view.findViewById(R.id.btn_sign_out);
        btnSignOut.setOnClickListener(this);
        tvName = view.findViewById(R.id.tv_name);
        ivProfile = view.findViewById(R.id.iv_profile);
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();
        sharedPreferences = getActivity().getSharedPreferences("userProfile", Context.MODE_PRIVATE);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);

        getProfile();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        getProfile();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit_profile:
                startActivity(new Intent(getContext(), UpdateProfileActivity.class));
                break;
            case R.id.btn_complain:
                startActivity(new Intent(getContext(), ComplainActivity.class));
                break;
            case R.id.btn_about_us:
                startActivity(new Intent(getContext(), AboutUsActivity.class));
                break;
            case R.id.btn_sign_out:
                signOut();
                break;
        }
    }

    private void signOut() {
        auth.signOut();
        mGoogleSignInClient.signOut();
        LoginManager.getInstance().logOut();

        File sharedPreferenceFile = new File("/data/data/"+ getActivity().getPackageName()+ "/shared_prefs/");
        File[] listFiles = sharedPreferenceFile.listFiles();
        for (File file : listFiles) {
            file.delete();
        }

        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    private void getProfile() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        user = gson.fromJson(json, User.class);
        tvName.setText(user.getName());
        Glide.with(getContext()).load(user.getPhoto()).placeholder(R.drawable.ic_placeholder_profile).into(ivProfile);
    }
}
