package com.example.sportloka.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sportloka.R;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

public class ComplainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView titleBar;
    private Button backBtn, sendBtn;
    private AppCompatEditText etService, etComplain;
    private FirebaseFirestore mDatabase;
    private SharedPreferences sharedPreferences;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_complain);

        sharedPreferences = getSharedPreferences("userProfile", Context.MODE_PRIVATE);
        mDatabase = FirebaseFirestore.getInstance();

        titleBar = findViewById(R.id.title_bar);
        backBtn = findViewById(R.id.btn_back);
        sendBtn = findViewById(R.id.btn_send);
        etComplain = findViewById(R.id.et_complain);
        etService = findViewById(R.id.et_service);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        titleBar.setText("COMPLAIN");
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(this);
        sendBtn.setOnClickListener(this);
    }

    boolean validate_field(String service, String complain){
        //CHECK FIELD SERVICE
        if(service.isEmpty()){
            etService.setError("Please enter your service");
            etService.requestFocus();
            return false;
        }

        //CHECK FIELD COMPLAIN
        if(complain.isEmpty()){
            etComplain.setError("Please enter your complain");
            etComplain.requestFocus();
            return false;
        }

        return  true;
    }

    private void sendComplain(String service, String complain) {
        progressBar.setVisibility(View.VISIBLE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        User user = gson.fromJson(json, User.class);

        final Map<String, Object> com = new HashMap<>();
        com.put("email", user.getEmail());
        com.put("service", service);
        com.put("complain", complain);

        mDatabase.collection("Complain")
                .add(com)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        //Save profile to local preferences
                        Toast.makeText(ComplainActivity.this, "Send Complain Success", Toast.LENGTH_SHORT).show();
                        etComplain.setText("");
                        etService.setText("");
                        progressBar.setVisibility(View.GONE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ComplainActivity.this, "Send Complain Failed, Please Send Again", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
               super.onBackPressed();
               break;
            case R.id.btn_send:
                String service = etService.getText().toString().trim();
                String complain = etComplain.getText().toString().trim();

                if (validate_field(service, complain)) {
                    etComplain.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    sendComplain(service, complain);
                }
                break;
        }
    }
}
