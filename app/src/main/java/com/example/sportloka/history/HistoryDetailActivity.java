package com.example.sportloka.history;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.sportloka.R;
import com.example.sportloka.models.Booking;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class HistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private Booking booking;
    private FirebaseFirestore mDatabase;
    private TextView tvDate, tvSchedule, tvLocation, tvPrice, tvStatus, tvNote, tvPay, tvAccount;
    private Button btnRating, btnCancel, btnBack;
    private ImageView image;
    private ProgressBar progressBar;
    private Gson gson = new Gson();
    private DecimalFormat kursIndonesia;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_booking);

        image = findViewById(R.id.iv_area);
        tvDate = findViewById(R.id.tv_date);
        tvSchedule = findViewById(R.id.tv_schedule);
        tvLocation = findViewById(R.id.tv_venue);
        tvPrice = findViewById(R.id.tv_price_hour);
        tvStatus = findViewById(R.id.tv_status);
        tvNote = findViewById(R.id.tv_note);
        tvPay = findViewById(R.id.tv_price_pay);
        tvAccount = findViewById(R.id.tv_account);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        btnRating = findViewById(R.id.btn_rating);
        btnRating.setOnClickListener(this);
        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        mDatabase = FirebaseFirestore.getInstance();

        Bundle dataParse = getIntent().getExtras();
        String json = dataParse.getString("booking");
        booking = gson.fromJson(json, Booking.class);

        kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        setupView();
    }

    private void setupView() {
        StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl("gs://sportloka-81187.appspot.com/").child(booking.getIdArea()+ ".jpg");
        storageReference.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Glide.with(getApplicationContext()).load(task.getResult()).placeholder(R.drawable.placeholder_sport).into(image);
                }
            }
        });
        tvDate.setText(booking.getDateBooking());

        String day = booking.getSchedule().replace("\"", "");
        String day1 = day.replace("[", "");
        String day2 = day1.replace("]", "");
        String mDay = day2.replace(",", ", ");
        tvSchedule.setText(mDay);

        tvLocation.setText(booking.getLoc());
        tvPrice.setText(String.valueOf(kursIndonesia.format(Double.valueOf(booking.getPrice()))));
        tvStatus.setText(booking.getStatus());
        tvNote.setText(booking.getNote());
        tvAccount.setText(booking.getAccount());


        Random r = new Random();
        int number = r.nextInt(999 - 1) + 1;
        String payDot = String.valueOf((Long.valueOf(booking.getPrice()) * 0.25) + number);
        String[] pay = payDot.split("\\.");


        if (booking.getStatus().equals("waiting payment") || booking.getStatus().equals("success booking")){
            tvPay.setText(String.valueOf(kursIndonesia.format(Double.valueOf(pay[0]))));
        } else {
            tvPay.setText("-");
        }
    }

    private void sendRating() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_rating, null);
        final RatingBar ratingBar = dialogLayout.findViewById(R.id.ratingBar);
        builder.setView(dialogLayout);
        builder.setTitle("RATE THE FIELD")
        builder.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressBar.setVisibility(View.VISIBLE);
                mDatabase.collection("Area")
                        .whereEqualTo("idArea", booking.getIdArea())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                if (task.isSuccessful()) {

                                    for (QueryDocumentSnapshot document : task.getResult()) {

                                        String idDoc = document.getId();

                                        String rating = document.getData().get("rating").toString();
                                        String reviewTotal = document.getData().get("reviewTotal").toString();

                                        float totalRating = Float.valueOf(rating) * Float.valueOf(reviewTotal);
                                        float tempRating = totalRating + ratingBar.getRating();
                                        int newReview = Integer.valueOf(reviewTotal) + 1;
                                        float newRating = tempRating / newReview;

                                        final Map<String, Object> rate = new HashMap<>();
                                        rate.put("rating", newRating);
                                        rate.put("reviewTotal", newReview);

                                        mDatabase.collection("Area")
                                                .document(idDoc)
                                                .update(rate)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            progressBar.setVisibility(View.GONE);
                                                            Toast.makeText(getApplicationContext(), "Rating Success", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            progressBar.setVisibility(View.GONE);
                                                            Toast.makeText(getApplicationContext(), "Rating failed", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
            }
        });
        builder.show();
    }

    private void cancelBooking() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setMessage("Do you really want to cancel?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        progressBar.setVisibility(View.VISIBLE);
                        mDatabase.collection("Booking")
                                .whereEqualTo("idBooking", booking.getIdBooking())
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                        if (task.isSuccessful()) {

                                            for (QueryDocumentSnapshot document : task.getResult()) {

                                                String idDoc = document.getId();

                                                final Map<String, Object> status = new HashMap<>();
                                                status.put("status", "Canceled");

                                                mDatabase.collection("Booking")
                                                        .document(idDoc)
                                                        .update(status)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    tvStatus.setText("Canceled");
                                                                    tvPay.setText("-");
                                                                    progressBar.setVisibility(View.GONE);
                                                                    Toast.makeText(getApplicationContext(), "Cancel Booking Success", Toast.LENGTH_SHORT).show();
                                                                } else {
                                                                    progressBar.setVisibility(View.GONE);
                                                                    Toast.makeText(getApplicationContext(), "Cancel Booking failed", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                            }
                                        } else {
                                            progressBar.setVisibility(View.GONE);
                                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                                        }

                                    }
                                });
                    }
                });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                cancelBooking();
                break;
            case R.id.btn_rating:
                sendRating();
                break;
            case R.id.btn_back:
                super.onBackPressed();
                break;
        }
    }
}
