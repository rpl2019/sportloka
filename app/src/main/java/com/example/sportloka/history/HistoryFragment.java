package com.example.sportloka.history;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sportloka.R;
import com.example.sportloka.models.Area;
import com.example.sportloka.models.Booking;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class HistoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private HistoryListAdapter adapter;
    private ArrayList<Booking> historyList;
    private FirebaseFirestore mDatabase;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private SharedPreferences sharedPreferences;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        progressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.rv_list_booking);

        progressBar.setVisibility(View.VISIBLE);

        adapter = new HistoryListAdapter(historyList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();


        sharedPreferences = getActivity().getSharedPreferences("userProfile", Context.MODE_PRIVATE);

        getData();
    }

    private void getData() {

        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", null);
        user = gson.fromJson(json, User.class);

        historyList = new ArrayList<>();

        mDatabase.collection("Booking")
                .whereEqualTo("email", user.getEmail())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            System.out.println(String.valueOf(task.getResult().size()));
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String date = document.getData().get("date").toString();
                                String email = document.getData().get("email").toString();
                                String idArea = document.getData().get("idArea").toString();
                                String idBooking = document.getData().get("idBooking").toString();
                                String latitude = document.getData().get("latitude").toString();
                                String longitude = document.getData().get("longitude").toString();
                                String loc = document.getData().get("location").toString();
                                String nameArea = document.getData().get("nameArea").toString();
                                String schedule = document.getData().get("schedule").toString();
                                String photo = document.getData().get("photo").toString();
                                String dateBooking = document.getData().get("dateBooking").toString();
                                String status = document.getData().get("status").toString();
                                String note = document.getData().get("note").toString();
                                String price = document.getData().get("price").toString();
                                String rekening = document.getData().get("rekening").toString();

                                Booking b = new Booking(date, email, idArea, idBooking, latitude, longitude, loc, nameArea, schedule, photo, dateBooking, status, note, price, rekening);
                                historyList.add(b);
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                });
    }
}
