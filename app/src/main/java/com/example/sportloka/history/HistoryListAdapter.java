package com.example.sportloka.history;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sportloka.home.detailArea.DetailAreaActivity;
import com.example.sportloka.models.Booking;
import java.util.ArrayList;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import com.example.sportloka.R;
import com.google.gson.Gson;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.HistoryListViewHolder> {

    private ArrayList<Booking> dataList;

    public HistoryListAdapter(ArrayList<Booking> dataList) {
        this.dataList = dataList;
    }

    @Override
    public HistoryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_history, parent, false);
        return new HistoryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryListViewHolder holder, final int position) {
        String[] time = dataList.get(position).getDate().split("\\s+");
        holder.areaName.setText(dataList.get(position).getNameArea());
        holder.areaLocation.setText(dataList.get(position).getLoc());
        holder.date.setText(time[0]);
        holder.time.setText(time[1]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String json = gson.toJson(dataList.get(position));
                v.getContext().startActivity(new Intent(v.getContext(), HistoryDetailActivity.class).putExtra("booking", json));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HistoryListViewHolder extends RecyclerView.ViewHolder{

        private CircleImageView image;
        private TextView areaName, areaLocation, date, time;
        private View statusActive;

        public HistoryListViewHolder(View itemView) {
            super(itemView);

            areaName = itemView.findViewById(R.id.tv_area_name);
            areaLocation = itemView.findViewById(R.id.tv_area_location);
            date = itemView.findViewById(R.id.tv_date);
            time = itemView.findViewById(R.id.tv_time);
            image = itemView.findViewById(R.id.iv_area);
        }
    }
}
