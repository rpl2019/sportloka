package com.example.sportloka.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sportloka.MainActivity;
import com.example.sportloka.R;
import com.example.sportloka.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;


public class RegisterActivity extends AppCompatActivity {

    private AppCompatEditText etEmail, etPassword, etRePassword, etName, etPhone;
    private Button btnSignUp;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mDatabase;
    private ProgressBar progressBar;
    private SharedPreferences sharedPreferences;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_register);
        etName = findViewById(R.id.et_name);
        etEmail = findViewById(R.id.et_email);
        etPhone = findViewById(R.id.et_phone);
        etPassword = findViewById(R.id.et_password);
        etRePassword = findViewById(R.id.et_re_password);
        progressBar = findViewById(R.id.progressBar);
        btnSignUp = findViewById(R.id.btn_signup);
        progressBar.setVisibility(View.GONE);

        //firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        sharedPreferences = getSharedPreferences("userProfile", Context.MODE_PRIVATE);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString().trim();
                String email= etEmail.getText().toString().trim();
                String phone = etPhone.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                String rePassword = etRePassword.getText().toString().trim();


                if(validate_field(name, email, phone, password, rePassword)){
                    etPhone.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    register(name, email, phone, password);
                }
            }
        });

    }

    boolean validate_field(String name, String email, String phone, String password, String rePassword){
        //CHECK FIELD EMAIL
        if(email.isEmpty()){
            etEmail.setError("Please enter your email");
            etEmail.requestFocus();
            return false;
        }

        else if(!EMAIL_ADDRESS_PATTERN.matcher(email).matches()){
            etEmail.setError("Please enter a valid email");
            etEmail.requestFocus();
            return false;
        }else {
            etEmail.setError(null);
        }

        //CHECK FIELD PASSWORD
        if(password.isEmpty()){
            etPassword.setError("Please enter a password");
            etPassword.requestFocus();
            return false;
        }
        else if(password.length() < 6){
            etPassword.setError("Password must contain 6 alpha-numeric");
            etPassword.requestFocus();
            return false;
        }else{
            etPassword.setError(null);
        }

        //CHECK RETYPE PASSWORD
        if (rePassword.isEmpty()) {
            etRePassword.setError("Please enter a re-type password");
            etRePassword.requestFocus();
            return false;
        } else if (!rePassword.equals(password)) {
            etRePassword.setError("Password not same");
            etRePassword.requestFocus();
            return false;
        } else {
            etRePassword.setError(null);
        }

        //CHECK FIELD NAME
        if(name.isEmpty()){
            etName.setError("Please enter your name");
            etName.requestFocus();
            return false;
        }else{
            etName.setError(null);
        }

        //CHECK FIELD PHONE
        if(phone.isEmpty()){
            etPhone.setError("Please enter your phone number");
            etPhone.requestFocus();
            return  false;
        }

        else if(phone.length() != 12){
            etPhone.setError("Please enter a valid phone number");
            etPhone.requestFocus();
            return  false;
        }else {
            etPhone.setError(null);
        }

        return  true;
    }

    private void register(final String name, final String email, final String phone, String password) {
        progressBar.setVisibility(View.VISIBLE);

        final Map<String, Object> user = new HashMap<>();
        user.put("name", name);
        user.put("email", email);
        user.put("phone", phone);
        user.put("register", "email");
        user.put("photo", "");

        final User userProfile = new User(name, email, phone, "email", "");

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Register Failed : " + task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_SHORT).show();
                            mDatabase.collection("User")
                                    .add(user)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            //Save profile to local preferences
                                            saveProfile(userProfile);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(RegisterActivity.this, "Save Profile Failed, Please Login Again", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                });
    }

    private void saveProfile(User user) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.apply();

        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
        finish();
    }
}
