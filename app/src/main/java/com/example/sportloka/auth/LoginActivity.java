package com.example.sportloka.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sportloka.MainActivity;
import com.example.sportloka.R;
import com.example.sportloka.models.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    //Deklarasi Variable
    private SignInButton btnGoogleSignIn;
    private Button btnSignIn, btnSignUp;
    private LoginButton btnFbSignIn;
    private AppCompatEditText etEmail, etPassword;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private FirebaseFirestore mDatabase;
    private SharedPreferences sharedPreferences;
    private User user;
    private final static int RC_SIGN_IN = 123;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FIREBASE
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);

        //Inisialisasi Widget
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        btnGoogleSignIn = findViewById(R.id.btn_signin_google);
        btnGoogleSignIn.setOnClickListener(this);
        btnFbSignIn = findViewById(R.id.btn_signin_fb);
        btnFbSignIn.setOnClickListener(this);
        btnSignIn = findViewById(R.id.btn_signin);
        btnSignIn.setOnClickListener(this);
        btnSignUp = findViewById(R.id.btn_signup);
        btnSignUp.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        sharedPreferences = getSharedPreferences("userProfile", Context.MODE_PRIVATE);

        //LOGIN GOOGLE
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //LOGIN FACEBOOK
        callbackManager = CallbackManager.Factory.create();
        btnFbSignIn.setReadPermissions("email", "public_profile");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("Google Sign In", "Google sign in failed", e);
                // ...
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleFacebookAccessToken(final AccessToken token) {
        progressBar.setVisibility(View.VISIBLE);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            getProfileFacebook(token);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "Authentication failed : " + task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        progressBar.setVisibility(View.VISIBLE);

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        user = new User(account.getDisplayName(), account.getEmail(), "", "google", account.getPhotoUrl().toString());

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mDatabase.collection("User")
                                    .whereEqualTo("email", user.getEmail())
                                    .whereEqualTo("register", user.getTypeLogin())
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                                                String idDoc = null;
                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                    idDoc = document.getId();
                                                }

                                                if (idDoc == null) {
                                                    registerUser(user);
                                                } else {
                                                    saveProfile(user);
                                                }
                                            } else {
                                                progressBar.setVisibility(View.GONE);
                                                Toast.makeText(LoginActivity.this, "auth Failed, Please Login Again", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "auth Failed, Please Login Again", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    private void saveProfile(User user) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.apply();

        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void getProfile(String email, String typeLogin) {
        mDatabase.collection("User")
                .whereEqualTo("email", email)
                .whereEqualTo("register", typeLogin)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String name = document.getData().get("name").toString();
                                String email = document.getData().get("email").toString();
                                String phone = document.getData().get("phone").toString();
                                String typeLogin = document.getData().get("register").toString();
                                String photo = document.getData().get("photo").toString();
                                user = new User(name, email, phone, typeLogin, photo);

                                saveProfile(user);
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Error Get Profile, Please Login Again", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void registerUser(final User userProfile) {
        final Map<String, Object> userData = new HashMap<>();
        userData.put("name", userProfile.getName());
        userData.put("email", userProfile.getEmail());
        userData.put("phone", userProfile.getPhone());
        userData.put("register", userProfile.getTypeLogin());
        userData.put("photo", userProfile.getPhoto());

        mDatabase.collection("User")
                .add(userData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        saveProfile(userProfile);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "auth Failed : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getProfileFacebook(AccessToken token) {
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            String email = response.getJSONObject().getString("email");
                            String firstName = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");

                            Profile profile = Profile.getCurrentProfile();
                            Uri uriImageProfile = profile.getProfilePictureUri(150, 150);
//                            if (Profile.getCurrentProfile()!=null)
//                            {
//                                Log.i("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
//                            }

                            user = new User(firstName+" "+lastName, email, "", "facebook", uriImageProfile.toString());

                            mDatabase.collection("User")
                                    .whereEqualTo("email", user.getEmail())
                                    .whereEqualTo("register", user.getTypeLogin())
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                                                String idDoc = null;
                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                    idDoc = document.getId();
                                                }

                                                if (idDoc == null) {
                                                    registerUser(user);
                                                } else {
                                                    saveProfile(user);
                                                }
                                            } else {
                                                progressBar.setVisibility(View.GONE);
                                                Toast.makeText(LoginActivity.this, "auth Failed, Please Login Again", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //VALIDATION FIELD
    boolean validate_field(String email, String password){
        //CHECK FIELD EMAIL
        if(email.isEmpty()){
            etEmail.setError("Please enter your email");
            etEmail.requestFocus();
            return false;
        }

        else if(!EMAIL_ADDRESS_PATTERN.matcher(email).matches()){
            etEmail.setError("Please enter a valid email");
            etEmail.requestFocus();
            return false;
        }else {
            etEmail.setError(null);
        }

        //CHECK FIELD PASSWORD
        if(password.isEmpty()){
            etPassword.setError("Please enter a password");
            etPassword.requestFocus();
            return false;
        }
        else if(password.length() < 6){
            etPassword.setError("Password must contain 6 alpha-numeric");
            etPassword.requestFocus();
            return false;
        }else{
            etPassword.setError(null);
        }

        return  true;
    }

    //Method ini digunakan untuk proses autentikasi user menggunakan email dan kata sandi
    private void loginEmail(final String email, String password){
        progressBar.setVisibility(View.VISIBLE);

        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Login Success", Toast.LENGTH_SHORT).show();
                            getProfile(email, "email");
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Authentication failed : " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void loginGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_signup:
                startActivity(new Intent(this, RegisterActivity.class));
                break;

            case R.id.btn_signin:
                String email = etEmail.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (validate_field(email, password)) {
                    etPassword.onEditorAction(EditorInfo.IME_ACTION_DONE);
                    loginEmail(email, password);
                }
                break;
            case R.id.btn_signin_google:
                loginGoogle();
                break;
            case R.id.btn_signin_fb:
                btnFbSignIn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("login facebook", "facebook:onSuccess:" + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.d("login facebook", "facebook:onCancel");
                        // ...
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("login facebook", "facebook:onError", error);
                        // ...
                    }
                });
                break;
        }
    }
}