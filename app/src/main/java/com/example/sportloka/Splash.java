package com.example.sportloka;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.example.sportloka.auth.LoginActivity;

import androidx.appcompat.app.AppCompatActivity;


public class Splash extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_splashcreen);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        }, 2000L); //3000 L = 3 detik
    }
}